const nodemailer = require('nodemailer');

const hbs = require('nodemailer-express-handlebars');
module.exports = {
  friendlyName: 'Send mail',
  description: '',
  inputs: {
    options: {
      type: 'ref',
      required: true,
    },
  },
  exits: {
    success: {
      description: 'All done.',
    },
  },
  fn: async function (inputs) {
    //test account
    let testAccount = await nodemailer.createTestAccount();

    const transporter = nodemailer.createTransport({
      service: 'gmail.com',
      auth: {
          user: 'macbrill13@gmail.com',
          pass: 'Oluwaseun2@'
      }
    });

    transporter.use(
      'compile',
      hbs({
        viewEngine: {
          extName: '.hbs',
          partialsDir: './views',
          layoutsDir: './views',
          defaultLayout: '',
        },
        viewPath: './views/',
        extName: '.hbs',
      })
    );
    try {
      console.log('im here, mail sent ', { ...inputs.options });
      let emailOptions = {
        from: 'CligoHR <tayoola13@yahoo.com>',
        ...inputs.options
      };
      await transporter.sendMail(emailOptions);
    } catch (error) {
      sails.log(error);
    }
  },
};
